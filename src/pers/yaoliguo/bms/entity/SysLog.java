package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName:       SysLog
 * @Description:    记录日志的实体类
 * @author:            yao
 * @date:            2017年6月5日        下午8:00:56
 */
public class SysLog implements Serializable{
	
	//32为uuid作为主键
	private String id;
	
	//user表id
	private String userId;
	
	//user名
	private String userName;
	
	//模块名称
	private String moudle;
	
	//操作
	private String operation;
	
	//创建时间
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMoudle() {
		return moudle;
	}

	public void setMoudle(String moudle) {
		this.moudle = moudle;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	

}
