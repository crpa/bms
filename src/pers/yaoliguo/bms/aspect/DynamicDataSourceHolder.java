package pers.yaoliguo.bms.aspect;
/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年7月31日 下午3:42:13 
* 类说明 
*/
public class DynamicDataSourceHolder {
	 private static final ThreadLocal<String> THREAD_DATA_SOURCE = new ThreadLocal<String>();

	    public static String getDataSource() {
	        return THREAD_DATA_SOURCE.get();
	    }

	    public static void setDataSource(String dataSource) {
	        THREAD_DATA_SOURCE.set(dataSource);
	    }

	    public static void clearDataSource() {
	        THREAD_DATA_SOURCE.remove();
	    }
}
