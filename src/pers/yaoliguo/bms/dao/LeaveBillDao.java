package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.leaveBill;
@Repository("leaveBillDao")
public interface LeaveBillDao {
	
    int deleteByPrimaryKey(String id);

    int insert(leaveBill record);

    int insertSelective(leaveBill record);

    leaveBill selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(leaveBill record);

    int updateByPrimaryKey(leaveBill record);
    
    List<leaveBill> selectAll(Map map);
}