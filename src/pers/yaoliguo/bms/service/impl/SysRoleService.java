package pers.yaoliguo.bms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pers.yaoliguo.bms.dao.SysRoleDao;
import pers.yaoliguo.bms.entity.SysRole;
import pers.yaoliguo.bms.service.ISysRoleService;
import pers.yaoliguo.bms.uitl.MapUtils;

/**
 * @ClassName:       SysRoleService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年7月4日        下午10:32:59
 */
@Service("sysRoleService")
@Transactional
public class SysRoleService implements ISysRoleService {

	@Autowired
	SysRoleDao sysRoleDao;

	@Override
	public int deleteByPrimaryKey(String id) {
		
		return sysRoleDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysRole record) {
		
		return sysRoleDao.insert(record);
	}

	@Override
	public int insertSelective(SysRole record) {
		
		return sysRoleDao.insertSelective(record);
	}

	@Override
	public SysRole selectByPrimaryKey(String id) {
		
		return sysRoleDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysRole record) {
		
		return sysRoleDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysRole record) {
		
		return sysRoleDao.updateByPrimaryKey(record);
	}

	@Override
	public int selectCount(SysRole record) {
		Map map = MapUtils.returnMap(record);
		return sysRoleDao.selectCount(map);
	}

	@Override
	public List<SysRole> selectAll(SysRole record) {
		Map map = MapUtils.returnMap(record);
		return sysRoleDao.selectAll(map);
	}

	@Override
	public int updateByPID(SysRole record) {
		
		return sysRoleDao.updateByPID(record);
	}

	@Override
	public int removeChildren(SysRole record) {
		
		List<SysRole> list = selectChildren(record);
		for (SysRole sysRole : list) {
			sysRole.setDel(true);
			sysRoleDao.updateByPrimaryKeySelective(sysRole);
		}
		
		return list.size();
	}
	
	public List<SysRole> selectChildren(SysRole record){
		
		SysRole r = new SysRole();
		r.setPid(record.getId());
		r.setDel(false);
		
		List<SysRole> list = selectAll(r);
		List<SysRole> list1 = null;
		List<SysRole> listCount = new ArrayList<SysRole>(); 
		if(list.size() > 0){
			listCount.addAll(list);
			for (SysRole sysRole : list) {
				list1 = selectChildren(sysRole);
				if(list1.size() > 0)
				{
					sysRole.getChildren().addAll(list1);
				}
			}
		}
		
		return listCount;
	}

	@Override
	public List<SysRole> selectRolesByKey(SysRole record) {
		// TODO Auto-generated method stub
		return selectChildren(record);
	}
	
}
