package pers.yaoliguo.bms.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.dao.SysUserDao;
import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.service.ISysUserService;
import pers.yaoliguo.bms.uitl.MapUtils;

/**
 * @ClassName:       SysUserService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月21日        下午9:36:59
 */
@Service("sysUserService")
@Transactional
public class SysUserService implements ISysUserService {

	@Autowired
	SysUserDao sysUserDao;

	@Override
	public int deleteByPrimaryKey(String id) {
		
		return sysUserDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysUser record) {
		
		return sysUserDao.insert(record);
	}

	@Override
	public int insertSelective(SysUser record) {
		
		return sysUserDao.insertSelective(record);
	}

	@Override
	public SysUser selectByPrimaryKey(String id) {
		
		return sysUserDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysUser record) {
		
		return sysUserDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysUser record) {
	
		return sysUserDao.updateByPrimaryKey(record);
	}

	@Override
	public List<SysUser> selectAllByPage(SysUser record,PagerBean page) {
		Map user = MapUtils.returnMap(record);
		Map pager = MapUtils.returnMap(page);
		user.putAll(pager);
		return sysUserDao.selectAll(user);
	}

	 

	@Override
	public List<SysUser> selectAll(Map map) {
		
		return sysUserDao.selectAll(map);
	}

	@Override
	public List<SysUser> selectAll(SysUser record) {
		// TODO Auto-generated method stub
		Map user = MapUtils.returnMap(record);
		return sysUserDao.selectAll(user);
	}

	@Override
	public int selectCount(SysUser record, PagerBean page) {
		Map user = MapUtils.returnMap(record);
		Map pager = MapUtils.returnMap(page);
		user.putAll(pager);
		return sysUserDao.selectCount(user);
	}
	
	
	
	
}
