package pers.yaoliguo.bms.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.dao.LeaveBillDao;
import pers.yaoliguo.bms.entity.leaveBill;
import pers.yaoliguo.bms.service.ILeaveBillService;
import pers.yaoliguo.bms.uitl.MapUtils;

/**
 * @ClassName:       leaveBillService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年8月16日        下午9:22:14
 */
@Service("leaveBillService")
public class LeaveBillService implements ILeaveBillService {

	@Autowired
	LeaveBillDao leaveBillDao;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		
		return leaveBillDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(leaveBill record) {
		
		return leaveBillDao.insert(record);
	}

	@Override
	public int insertSelective(leaveBill record) {
		
		return leaveBillDao.insertSelective(record);
	}

	@Override
	public leaveBill selectByPrimaryKey(String id) {
		
		return leaveBillDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(leaveBill record) {
		
		return leaveBillDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(leaveBill record) {
		
		return leaveBillDao.updateByPrimaryKey(record);
	}

	@Override
	public List<leaveBill> selectAll(leaveBill record, PagerBean page) {
		if(record == null){
			record = new leaveBill();
		}
		if(page == null){
			page = new PagerBean();
		}
		Map map = MapUtils.returnMap(record);
		Map pager = MapUtils.returnMap(page);
		//map.putAll(pager);
		return leaveBillDao.selectAll(map);
	}

}
