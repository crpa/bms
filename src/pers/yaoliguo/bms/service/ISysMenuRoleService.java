package pers.yaoliguo.bms.service;

import java.util.List;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;

/**
 * 
 * @ClassName: ISysMenuRoleService 
 * @Description: TODO
 * @author: wangyi
 * @date: 2017年7月20日 下午2:07:20
 */
public interface ISysMenuRoleService {
	List<SysRoleMenuKey>getrolemenus(SysRoleMenuKey record);
	
	 int deleteByPrimaryKey(SysRoleMenuKey key);
	 
	 int insert(SysRoleMenuKey record);
	 
	 int deleteByRoleId(SysRoleMenuKey key);
}
